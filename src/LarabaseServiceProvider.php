<?php
namespace Rawveg\Larabase;

use Illuminate\Support\ServiceProvider;
use Rawveg\Larabase\App\Console\CreateCommand;
use Rawveg\Larabase\App\Console\StubPublishCommand;

class LarabaseServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands(
                [
                    CreateCommand::class,
                    StubPublishCommand::class,
                ]
            );
        }
    }

    public function provides()
    {
        return [
            CreateCommand::class,
            StubPublishCommand::class,
        ];
    }
}
