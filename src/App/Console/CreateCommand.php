<?php
namespace Rawveg\Larabase\App\Console;

use Illuminate\Console\Command;

class CreateCommand extends Command
{
    protected $signature = 'make:crud
                            {model : The name of the model}';
    protected $description = 'Creates CRUD setup including Model, Create and Update Requests, Api Resource, Controller and Test stubs';

    public function handle()
    {
        $modelName = $this->argument('model');
        $resourceName = $modelName.'Resource';
        $createRequest = 'Create'.$modelName;
        $updateRequest = 'Update'.$modelName;

        $this->call('make:model', [
            '--api' => true,
            '--all' => true,
            'name' => $modelName,
        ]);

        $this->call('make:resource', ['name' => $resourceName]);
        $this->call('make:request', ['name' => $createRequest]);
        $this->call('make:request', ['name' => $updateRequest]);
        $this->call('make:test', ['name' => $modelName."\\GetTest"]);
        $this->call('make:test', ['name' => $modelName."\\CreateTest"]);
        $this->call('make:test', ['name' => $modelName."\\UpdateTest"]);
        $this->call('make:test', ['name' => $modelName."\\DeleteTest"]);
        return 0;
    }
}
