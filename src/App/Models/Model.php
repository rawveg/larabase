<?php
namespace Rawveg\Larabase\App\Models;

use Rawveg\Larabase\App\Traits\ModelHelpers;
use Rawveg\Larabase\App\Traits\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

/**
 * Class Model
 * @package Rawveg\Larabase\App\Models
 * @method static Model find(string $uuid)
 * @method static Model findOrFail(string $uuid)
 * @method static Model isEnabled()
 * @method static Model isVisible()
 * @property mixed ignoreFields
 */
class Model extends EloquentModel
{
    use HasFactory, SoftDeletes, Uuid, ModelHelpers;

    public const RuleModel = false;
    protected $keyType = 'uuid';
    public $incrementing = false;
    public Request $request;

    public static function isRuleModel(): bool
    {
        return static::RuleModel;
    }

    /**
     * @param  \Illuminate\Http\Request|null  $request
     * @return static
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public static function getInstance(?Request $request = null): self
    {
        $instance = app()->make(self::class);
        if (!empty($request)) {
            $instance->request = $request;
        }
        return $instance;
    }

}
