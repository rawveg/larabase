<?php
namespace Rawveg\Larabase\App\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Schema;

trait ModelHelpers
{
    /**
     * Returns fillable fields, to skip a field add it to the ignoreFields property
     * @return array
     */
    public static function getFillableFields(): array
    {
        $instance = new static();
        $fillable = $instance->getFillable();
        if (!empty($fillable)) {
            $ignore = $instance->ignoreFillableFields();
            if (!empty($ignore)) {
                $fillable = array_diff($fillable, $ignore);
            }
        }
        return $fillable;
    }

    /**
     * @return array
     */
    protected function ignoreFillableFields(): array
    {
        return $this->ignoreFields;
    }

    /**
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIsEnabled(Builder $query): Builder
    {
        return ($this->columnExists('enabled')) ?
            $query->where('enabled', '=', true) : $query;
    }

    /**
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIsVisible(Builder $query): Builder
    {
        return ($this->columnExists('visible')) ?
            $query->where('visible', '=', true) : $query;
    }

    /**
     * @param  string  $columnName
     * @return bool
     */
    public function columnExists(string $columnName): bool
    {
        return (Schema::connection($this->getConnectionName())
            ->hasColumn($this->getTable(), $columnName)
        );
    }
}
