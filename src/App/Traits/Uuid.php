<?php

namespace Rawveg\Larabase\App\Traits;

use Illuminate\Support\Str;

trait Uuid
{
    protected static function boot()
    {
        parent::boot();

        static::creating(
            function ($model) {
                $model->id = (string)Str::uuid(); // generate uuid
            }
        );
    }
}
