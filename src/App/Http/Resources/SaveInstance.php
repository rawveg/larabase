<?php

namespace Rawveg\Larabase\App\Http\Resources;

class SaveInstance extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            $this->resource->getKeyName() => $this->resource->getKey(),
        ];
    }
}
