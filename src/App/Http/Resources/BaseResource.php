<?php

namespace Rawveg\Larabase\App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\URL;

class BaseResource extends JsonResource
{
    public function __construct($resource)
    {
        parent::__construct($resource);
        $this->additional(
            [
                'meta' => [
                    'current' => URL::current(),
                ]
            ]
        );
    }
}
