<?php
namespace Rawveg\Larabase\App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Rawveg\Larabase\App\Http\Resources\SaveInstance;

/**
 * Class LarabaseApiController
 * @package Rawveg\Larabase\App\Http\Controllers
 */
class LarabaseApiController extends Controller
{
    protected string $model;
    protected string $resource;
    protected array $requests = [];
    protected array $disabledMethods = [];

    public function index()
    {
        $this->__isDisabled(__FUNCTION__);
        return $this->__getResource()::collection($this->model::isEnabled()->get());
    }

    public function show()
    {
        $this->__isDisabled(__FUNCTION__);
        $instance = $model = $this->__processInstance(func_get_arg(0));
        $class = $this->__getResource();
        return app()->make($class, [
            'resource' => $instance,
        ]);
    }

    public function store(Request $request)
    {
        $this->__isDisabled(__FUNCTION__);
        $request = $this->__processRequest('create', $request);
        $instance = app()->make($this->model, [
            'attributes' => $request->only($this->model::getFillableFields())
        ]);
        $instance->save();
        return new SaveInstance($instance);
    }

    public function update(Request $request, $instance)
    {
        $this->__isDisabled(__FUNCTION__);
        $request = $this->__processRequest('update', $request);
        $model = $this->__processInstance($instance);
        $request = $this->__patchJson($request, $model);
        $model->fill($request->only($this->model::getFillableFields()));
        $model->save();
        return new SaveInstance($model);
    }

    public function destroy()
    {
        $this->__isDisabled(__FUNCTION__);
        $model = $this->__processInstance(func_get_arg(0));
        $model->delete();
        return response()->noContent();
    }

    protected function __processRequest($method, $argument)
    {
        if (!array_key_exists($method, $this->requests)) {
            $this->requests[$method] = $this->__getRequest($method);
        }
        $request = Request::createFrom($argument, new $this->requests[$method]);
        if (method_exists($request, 'rules') && method_exists($request, 'messages')) {
            $request->validate($request->rules(), $request->messages());
        } else if (method_exists($request, 'rules')) {
            $request->validate($request->rules());
        }
        return $request;
    }

    protected function __processInstance($argument)
    {
        $instance = $argument;
        if (!(gettype($argument) === $this->model)) {
            $instance = $this->model::findOrFail($argument);
        }
        return $instance;
    }

    protected function __getNameElement(): string
    {
        $parts = explode('\\', $this->__getModel());
        return end($parts);
    }

    /**
     * Get Model Class
     * @return string
     * @throws \Exception
     */
    protected function __getModel(): string
    {
        if (is_null($this->model)) {
            throw new \Exception('No Model Specified');
        }
        return $this->model;
    }

    /**
     * Get API Resource class
     * @return string
     */
    protected function __getResource(): string
    {
        if (!empty($this->resource)) {
            return $this->resource;
        }
        $prefix = $this->__getNameElement();
        $resource = 'App\\Http\\Resources\\' . $prefix . 'Resource';
        return (class_exists($resource)) ? $resource : JsonResource::class;
    }

    /**
     * Get Request class
     * @param $method
     * @return string
     */
    protected function __getRequest($method): string
    {
        $prefix = $this->__getNameElement();
        $request = 'App\\Http\\Requests\\' . ucfirst($method) . Str::singular($prefix);
        $requests = 'App\\Http\\Requests\\' . ucfirst($method) . Str::plural($prefix);
        $requestToUse = class_exists($requests) ? $requests : $request;
        return class_exists($requestToUse) ? $requestToUse : Request::class;
    }

    /**
     * Specify which methods are disabled
     * @param string|array $methodName
     * @return $this
     */
    protected function disableMethod($methodName): self
    {
        if (is_array($methodName)) {
            $this->disabledMethods = $methodName;
            return $this;
        }
        $this->disabledMethods[] = $methodName;
        return $this;
    }

    /**
     * Check whether or not a method is enabled
     * @param string $methodName
     * @return bool
     * @throws \Exception
     */
    protected function __isDisabled(string $methodName): bool
    {
        if (in_array($methodName, $this->disabledMethods)) {
            throw new \Exception('PHP Fatal error:  Call to undefined method '.$methodName.'()');
        }
        return false;
    }

    /**
     * Do the equivalent of a PATCH request on JSON data
     * @param  \Illuminate\Http\Request  $request
     * @param $instance
     * @return \Illuminate\Http\Request
     */
    protected function __patchJson(Request $request, $instance): Request
    {
        foreach($request->all() as $key => $value) {
            if ($this->__getColumnType($instance, $key, $value) === 'json') {
                $newJson = json_encode(
                    array_merge(
                        json_decode($instance->$key, true),
                        json_decode($value, true)
                    )
                );
                $request->request->set($key, $newJson);
            }
        }
        return $request;
    }

    /**
     * Get Column Type
     * @param $instance
     * @param $column
     * @return string
     */
    protected function __getColumnType($instance, $column, $value): string
    {
        $type = strtolower(
            DB::getSchemaBuilder()->getColumnType($instance->getTable(), $column)
        );
        if ($type === 'text') {
            if ($this->__isJson($value)) {
                $type = 'json';
            }
        }
        return $type;
    }

    /**
     * Is string JSON?
     * @param $string
     * @return bool
     */
    protected function __isJson($string): bool
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}
